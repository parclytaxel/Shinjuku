from shinjuku import lt
from .transforms import oriens, ocodes, inv_ocodes
# Gliders in all orientations and all phases. Groups of four are SE, SW, NW and NE respectively
# and phase advances within each group.
gliders = [lt.pattern(x) for x in ["bo$2bo$3o", "obo$b2o$bo", "2bo$obo$b2o", "o$b2o$2o",
"o$obo$2o", "2bo$2o$b2o", "bo$o$3o", "obo$2o$bo",
"3o$o$bo", "bo$2o$obo", "2o$obo$o", "b2o$2o$2bo",
"b2o$obo$2bo", "2o$b2o$o", "3o$2bo$bo", "bo$b2o$obo"]]

class gset:
    def __init__(self, ll):
        """Create a new glider set by setting this instance's list to ll,
        which should have exactly four Patterns."""
        self.l = ll
    def extract(comp):
        """Factory method that extracts the gliders from the Pattern comp.
        The gliders should be well-spaced for the matching to work."""
        res = []
        for block in zip(*[iter(gliders)] * 4):
            salvo = sum((comp.match(g, halo="3o$3o$3o").convolve(g) for g in block), start=lt.pattern())
            # Use this mask to manually remove in-constellation gliders from the glider set
            mask = lt.pattern() # lt.pattern("76o$" * 85)(116, 95)
            res.append(salvo - mask)
        return gset(res)
    
    @property
    def s(self):
        """Return the combination of all Patterns in this glider set."""
        return self.l[0] + self.l[1] + self.l[2] + self.l[3]
    
    def __getitem__(self, n):
        """Applies [n] to the individual salvos of this glider set. Since each Pattern
        is itself periodic, lifelib can rewind them on its own."""
        return gset([part[n] for part in self.l])
    
    def __call__(self, *args):
        """Applies (*args) to this glider set. Care must be taken to permute salvos in case of a rotation."""
        temp = self.l
        if len(args) & 1: # rotation
            o = args[0]
            r0, r1, r2, r3 = [part(o) for part in temp]
            if o == "identity": temp = [r0, r1, r2, r3]
            elif o == "rot90": temp = [r1, r2, r3, r0]
            elif o == "rot180": temp = [r2, r3, r0, r1]
            elif o == "rot270": temp = [r3, r0, r1, r2]
            elif o == "flip_y": temp = [r3, r2, r1, r0]
            elif o == "swap_xy_flip": temp = [r2, r1, r0, r3]
            elif o == "flip_x": temp = [r1, r0, r3, r2]
            elif o == "swap_xy": temp = [r0, r3, r2, r1]
        if len(args) >= 2: # translation
            temp = [part(args[-2], args[-1]) for part in temp]
        return gset(temp)
    
    def ngliders(self):
        """Return the number of gliders in each unidirectional salvo."""
        return [salvo.population // 5 for salvo in self.l]
    def __len__(self):
        """Return the number of gliders in this glider set as a whole."""
        return sum(self.ngliders())
    
    def centre(self):
        """Returns this glider set centred on its overall bounding box, following lifelib's definition."""
        bb = self.s.getrect()
        return self(-(bb[0] + bb[2] // 2), -(bb[1] + bb[3] // 2))
    
    def finite_lifetime(self):
        """Determines whether this glider set has finite lifetime."""
        for expo in range(64):
            separate = self[2 ** expo]
            if separate.s != self.s[2 ** expo]: return True
            # Compute margins between gliders travelling N/S and E/W
            # Empty such sets make the corresponding comparison True
            se, sw, nw, ne = separate.l
            nr, sr, er, wr = [d.getrect() for d in (nw + ne, se + sw, se + ne, sw + nw)]
            # South's upper bound - North's lower bound
            try: ns_margin = sr[1] - (nr[1] + nr[3] - 1)
            except TypeError: ns_margin = 999
            # East's left bound - West's right bound
            try: ew_margin = er[0] - (wr[0] + wr[2] - 1)
            except TypeError: ew_margin = 999
            # The gliders will never interact again if the condition below is true
            if ns_margin >= 3 and ew_margin >= 3: return False
        return False
    
    def canonical_time_finite(self):
        """Given that this glider set has finite lifetime, advance it to canonical time
        and return that alongside the number of generations advanced."""
        from itertools import count
        for i in count(1): # Advance individual gliders by i generations
            if self[i].s != self.s[i]: return (self[i - 1], i - 1)
    
    def margins(self):
        """Computes and returns (N/S margin, E/W margin) for this glider set,
        where the margin is the number of empty infinite lines between the relevant "factions".
        If no meaningful margin in a direction exists, return 1."""
        se, sw, nw, ne = self.l
        nr, sr, er, wr = [d.getrect() for d in (nw + ne, se + sw, se + ne, sw + nw)]
        # North's upper bound - South's lower bound
        try: ns_margin = nr[1] - (sr[1] + sr[3])
        except TypeError: ns_margin = 1
        # West's left bound - East's right bound
        try: ew_margin = wr[0] - (er[0] + er[2])
        except TypeError: ew_margin = 1
        return (ns_margin, ew_margin)
    
    def canonical_time_infinite(self):
        """Given that this glider set has infinite lifetime, advance it to canonical time
        and return that alongside the number of generations advanced (which may be negative)."""
        # Rewind until the margins are positive
        from itertools import count
        for expo in range(64):
            if min(self[-2 ** expo].margins()) > 0:
                delta = -2 ** expo
                break
        # Then advance until this is no longer true
        for i in count(delta + 1):
            if min(self[i].margins()) <= 0:
                return (self[i - 1], i - 1)
    
    def pairs(self):
        """Returns a representation of this set's gliders as a length-4 list of lists,
        where the entries in each sub-list are pairs (time, lane number) corresponding
        to the gliders in the sub-list's corresponding Pattern."""
        res = []
        for salvo in [self.l[0], self.l[1]("rot90"), self.l[2]("rot180"), self.l[3]("rot270")]:
            codes = [] # this direction's gliders
            for phase in range(4):
                for (x, y) in salvo.match(gliders[phase]).coords():
                    time = 1 - 4 * (x + 1) - (phase + 1) % 4
                    lane = x - y + (0 < phase < 3)
                    codes.append((time, lane))
            codes.sort()
            res.append(codes)
        return res

    def __str__(self):
        """Returns a string representation of this glider set based on pairs()."""
        from itertools import chain
        return "/".join(" ".join(str(n) for n in chain.from_iterable(direc)) for direc in self.pairs())

    def canonical_form(self):
        """Given that this glider set is at canonical time, returns the canonical form
        (orientation and origin included). Not intended for unidirectional sets."""
        return max([self(o).centre() for o in oriens], key=lambda x: x.pairs())

    def canonical_form_unidirectional(self):
        """Given that this glider set is unidirectional, returns the canonical form."""
        if   self.l[0]: t = self
        elif self.l[1]: t = self("rot90")
        elif self.l[2]: t = self("rot180")
        elif self.l[3]: t = self("rot270")
        return max([t[g](o).centre() for g in range(4) for o in ("identity", "swap_xy")], key=lambda x: x.pairs())

    def shift_test(self, cand):
        """Tests whether glider set cand is translatable to this glider set.
        If yes, returns (x, y) shift amounts; otherwise returns None."""
        selfr, candr = self.s.getrect(), cand.s.getrect()
        shift_x = selfr[0] - candr[0]
        shift_y = selfr[1] - candr[1]
        if cand(shift_x, shift_y).s == self.s: return (shift_x, shift_y)
        return None

    def ct(self):
        """Compute the Canonical form of this glider set and the accompanying Transformation FROM it,
        returning both as strings."""
        # Special procedures if this is unidirectional
        if sum(salvo.nonempty() for salvo in self.l) <= 1:
            cform = self.canonical_form_unidirectional()
            rn = range(4)
        else:
            if self.finite_lifetime(): ctime, t = self.canonical_time_finite()
            else: ctime, t = self.canonical_time_infinite()
            cform = ctime.canonical_form()
            rn = [t]
        for u in rn:
            for o in oriens:
                m = self.shift_test(cform[-u](o))
                if m: return (str(cform), f"@{u}{ocodes[o]}{m[0]} {m[1]}")

    def reconstruct(gstr):
        """Factory method that reconstructs a glider set from its (canonical) string.
        The transformation is assumed to be concatenated to the glider data."""
        fields, _, trans_str = gstr.partition("@")
        res = []
        # Process transformation
        if not trans_str:
            t, o, shift_x, shift_y = 0, "identity", 0, 0
        else:
            for c in "FLBRflbr":
                m = trans_str.partition(c)
                if m[1]:
                    t, o = int(m[0]), inv_ocodes[m[1]]
                    shift_x, shift_y = [int(x) for x in m[2].split()]
        for field in fields.split("/"):
            salvo = lt.pattern()
            for (time, lane) in zip(*[iter(field.split())] * 2):
                time, lane = int(time), int(lane)
                salvo += lt.pattern("bo$2bo$3o")(0, -lane)[-time - t - 4]
            if   len(res) == 1: salvo = salvo("rot270")
            elif len(res) == 2: salvo = salvo("rot180")
            elif len(res) == 3: salvo = salvo("rot90")
            res.append(salvo)
        return gset(res)(o, shift_x, shift_y)
