from .search import dijkstra, read_components
from .transcode import realise_comp, decode_comp, encode_comp
from .checks import rewind_check
from . import lt, lt4


def all_orientations(pat):
    if isinstance(pat, str):
        pat = lt.pattern(pat)

    pats = []

    for o in ["identity", "rot270", "rot180", "rot90", "flip_x", "flip_y", "swap_xy", "swap_xy_flip"]:
        pato = pat(o).centre()
        for p in pats:
            if p == pato:
                break
        else:
            pats.append(pato)

    return pats


def component_info_slonly(comp):
    if isinstance(comp, str):
        comp = lt.pattern(comp)

    x = comp & comp[1]
    x = x - (comp - x).convolve(lt.pattern('ooo$ooo$ooo!').centre())
    x = x & x[1]
    x = x & x[1]
    pop = (comp - x).population
    compcost = pop // 5
    return compcost, x.apgcode


def apply_tree(pats, tr, min_paths=None):
    """Applies the fragment tree in the second argument to one orientation
    of every pattern in the first argument. This minimises the number of
    calls to lifelib by assembling all of the targets into a mosaic and
    performing matches on the mosaic."""

    mosaic = lt.pattern()

    # Obtain integer square-root:
    modulus = int(len(pats) ** 0.5) - 1
    if modulus < 0:
        modulus = 0
    while modulus * modulus < len(pats):
        modulus += 1

    # Determine spacing of patterns:
    diam = 1
    for p in pats:
        bbox = p.bounding_box
        diam = max(diam, bbox[2], bbox[3])
    diam += 3
    log2d = len(bin(diam)) - 2
    radius = 1 << (log2d - 1)
    diam = radius + radius

    coords_to_apgcode = {}

    # Assemble mosaic:
    for (k, p) in enumerate(pats):
        i = (k % modulus)
        j = (k // modulus)
        lx = (i << log2d)
        ly = (j << log2d)
        mosaic += p(lx + radius, ly + radius)
        coords_to_apgcode[(lx, ly)] = p.apgcode

    sols = set()

    for (live, dead, replacements) in tr.values():
        m = mosaic.match(live=live, dead=dead)
        if m.empty():
            continue
        coords = map(tuple, m.coords())
        for (x, y) in coords:
            lx = (x >> log2d) << log2d
            x -= lx
            ly = (y >> log2d) << log2d
            y -= ly
            p = mosaic[lx: lx + diam, ly: ly + diam]
            p = p(-lx, -ly)
            output = coords_to_apgcode[(lx, ly)]

            for (n, r) in replacements:

                q1 = p - live(x, y)
                r1 = r(x, y)

                if (r1 & q1).nonempty():
                    continue

                q = q1 + r1

                if q[n + 1] != p:
                    continue

                try:
                    if min_paths is not None:
                        if output in min_paths:
                            compcost, input = component_info_slonly(q)
                            if input not in min_paths:
                                continue
                            if min_paths.get(input)[0] + compcost > min_paths.get(output)[0]:
                                continue

                    c = encode_comp(q, remove_spaceships=False)
                    if (c not in sols) and rewind_check(*realise_comp(c, separate=True)):
                        sols.add(c)
                except (KeyError, ValueError):
                    continue

    return sols


def get_useful_components():
    min_paths = dijkstra()
    return [v[2] for (k, v) in min_paths.items() if k.startswith('xs') and v[1].startswith('xs')]


def get_all_components():
    comps = read_components()
    result = []
    for c in comps:
        input, compcost, output = decode_comp(c)
        if input.startswith('xs') and output.startswith('xs'):
            result.append(c)
    return result


def components_to_triples(shinjuku_lines):
    s2s = set()

    for s in shinjuku_lines:
        sp = s.split('>')
        if (len(sp) >= 3) and (sp[0].startswith('xs')) and (sp[2].startswith('xs')) and (sp[0] != sp[2]):
            op = int(sp[0][2:].split('_')[0])
            fp = int(sp[2][2:].split('_')[0])
            s2s.add((fp - op, s))

    print("%d components to analyse." % len(s2s))

    triples = []
    wech_cache = set([])

    n9 = lt.pattern('3o$3o$3o!').centre()
    n21 = lt.pattern('b3o$5o$5o$5o$b3o!').centre()

    for (i, (m, s)) in enumerate(s2s):

        if i % 100 == 0:
            print("%d components analysed." % i)

        x = realise_comp(s, advance=-16)
        y = lt.pattern('')
        z = x
        n = 0

        while z != z[1]:
            y += (z ^ z[1])
            z = z[1]
            n += 1

        y = y.convolve(n9)

        x &= y
        z &= y

        if (z == x) or z.empty():
            continue
        wech2 = x.wechsler
        if wech2 in wech_cache:
            continue
        wech_cache.add(wech2)

        y &= z.convolve(n21)

        triples.append('%d:%d:%s' % (m, n, lt4.unify(z, y, x).wechsler))

    return triples


def triples_to_tree(triples, components=-9999):
    replacements = {}

    for triple in triples:

        ts = triple.split(':')
        m = int(ts[0])
        if m < components:
            continue

        n = int(ts[1])
        pat = lt4.pattern('xp0_' + ts[2], verify_apgcode=False)
        z, y, x = tuple(((lt.pattern() + a) for a in pat.layers()[:3]))

        fmatch = lt4.unify(z, y)
        wech = fmatch.wechsler
        fmatch2 = lt4.pattern('xp0_' + wech, verify_apgcode=False)
        z, y = tuple(((lt.pattern() + a) for a in fmatch2.layers()[:2]))

        for o in ["identity", "rot270", "rot180", "rot90", "flip_x", "flip_y", "swap_xy", "swap_xy_flip"]:
            fmatch3 = fmatch(o)
            if fmatch3.centre() == fmatch2.centre():
                x = x(o)
                b3 = fmatch3.bounding_box
                b2 = fmatch2.bounding_box
                x = x(b2[0] - b3[0], b2[1] - b3[1])
                break

        if lt4.unify(z, y, x).wechsler != ts[2]:
            raise ValueError("This should never happen!")

        if wech not in replacements:
            replacements[wech] = (z, y, [])

        replacements[wech][-1].append((n, x))

    return replacements


def synthesise_things(triples, objects, outfile=None, chunksize=64, min_paths=None):
    """Iterates over a sequence of apgcodes of still-lifes and saves easy
    syntheses to outfile. Takes < 1 second per object, amortized.

    triples: fragment tree, or list of triples, or filename;
    objects: list of apgcodes, or filename containing such a list;
    outfile: filename to export Shinjuku synthesis lines"""

    if isinstance(objects, str):
        if outfile is None:
            outfile = objects + '.sjk'
        with open(objects) as f:
            objects = [line.strip() for line in f]

    if isinstance(triples, str):
        with open(triples) as f:
            triples = [line.strip() for line in f]

    if isinstance(triples, list):
        triples = triples_to_tree(triples)

    objects = [line for line in objects if line.startswith('xs')]
    pats = [a for target in objects for a in all_orientations(target)]

    print('# Processing %d orientations' % len(pats))

    with open(outfile, 'w') as g:

        for i in range(0, len(pats), chunksize):
            j = min(i + chunksize, len(pats))

            apps = apply_tree(pats[i:j], triples, min_paths)
            for a in apps:
                g.write('%s\n' % a)
            print('# %d orientations complete' % j)
