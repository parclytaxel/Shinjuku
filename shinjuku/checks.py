from shinjuku import lt
from .search import read_components
from .transcode import realise_comp
moore = lt.pattern("3o$3o$3o")(-1, -1)

def check_single_line(line):
    base, glider_set = realise_comp(line, separate=True)
    if not rewind_check(base, glider_set):
        raise ValueError(f"{line} is not infinitely rewindable")
    pat = base[-2] + glider_set[-2].s
    out_params = pat.oscar(verbose=False, return_apgcode=True, maxexp=20)
    if not out_params:
        raise ValueError(f"input gliders of {line} do not produce a periodic object")
    out_apgcode = out_params["apgcode"]
    expected = line.split(">")[2]
    if not expected: expected = "xs0_0"
    if expected != out_apgcode:
        raise ValueError(f"{line} does not produce expected output: {expected} != {out_apgcode}")

def consistency_check(in_fn="", out_fn="", limit=0):
    """Ensure that Shinjuku's components do as they say, i.e. their stated result
    is consistent with the result obtained by realising the glider set and input,
    and the glider sets are infinitely rewindable. Raises ValueError otherwise.
    in_fn and out_fn can be used to implement a cache, as is done in the GitLab CI for Shinjuku.
    Specifying limit restricts the number of newly checked and written components."""
    lines = read_components(verbose=True)
    checked = read_components([in_fn])
    newcomps = lines - checked
    numchecked = min(limit, len(newcomps)) if limit > 0 else len(newcomps)
    print(f"not checking {len(checked)}; checking {numchecked}")
    for (n, line) in enumerate(newcomps, 1):
        check_single_line(line)
        checked.add(line)
        if n % 100 == 0:
            print(f"{n}/{numchecked}")
        if n >= limit > 0:
            break
    print("done")
    if out_fn != "":
        with open(out_fn, 'w') as f:
            for line in checked:
                f.write(f"{line}\n")
        print(f"wrote {len(checked)}")

cc = consistency_check

def disjoint_ranges_check(pos_gliders, constell, neg_gliders, direc, d, p):
    """Checks whether the termination condition of rewind_check is satisfied in
    the axis direc, which is either "x" or "y". pos_gliders/neg_gliders have
    positive/negative speed in this axis; constell's speed in this axis is d/p.
    The first three arguments are Patterns."""
    # Define the order in which the ranges will be placed and tested
    if 4 * abs(d) < p: order = [pos_gliders, constell, neg_gliders]
    elif 4 * d < -p: order = [pos_gliders, neg_gliders, constell]
    elif 4 * d > p: order = [constell, pos_gliders, neg_gliders]
    elif 4 * d == -p: order = [pos_gliders, constell + neg_gliders]
    elif 4 * d == p: order = [constell + pos_gliders, neg_gliders]
    # Get bounding boxes, expanded by one cell in each direction
    ranges = []
    for pat in order:
        if not pat: continue
        x, y, width, height = pat.convolve(moore).getrect()
        if direc == "x": ranges += [x, x + width - 1]
        elif direc == "y": ranges += [y, y + height - 1]
    # Check that ranges is sorted and all the numbers there are distinct
    return all(ranges[i] < ranges[i + 1] for i in range(len(ranges) - 1))
    
def rewind_check(base, glider_set):
    """Given a base constellation Pattern and a glider set, checks whether the latter
    can be rewound infinitely. Raises ValueError otherwise."""
    x, y = base.displacement
    p = base.period
    for expo in range(64):
        base_rw = base[-2 ** expo]
        glider_set_rw = glider_set[-2 ** expo]
        if (base_rw + glider_set_rw.s)[2 ** expo] != base + glider_set.s:
            return False
        se, sw, nw, ne = glider_set_rw.l
        c1 = disjoint_ranges_check(se + ne, base_rw, sw + nw, "x", x, p)
        c2 = disjoint_ranges_check(se + sw, base_rw, ne + nw, "y", y, p)
        if c1 and c2:
            return True
    return True
