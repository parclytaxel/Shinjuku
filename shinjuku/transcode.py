from shinjuku import lt
from .transforms import oriens, derive_tf
from .gliderset import gset

def comp_canonical_time(comp):
    """Splits and returns the glider set and the constellation of the component at canonical time.
    Assume the gliders are well-spaced and the lifetime is finite."""
    from itertools import count
    glider_set = gset.extract(comp)
    constell = comp - glider_set.s
    for i in count(1):
        advanced = glider_set[i].s
        if advanced != glider_set.s[i] or advanced + constell[i] != comp[i]:
            return (glider_set[i - 1], constell[i - 1])

def remove_standard_spaceships(comp, rewind=160):
    """Attempts to reasonably handle syntheses containing well-separated
    standard spaceships, reducing them to glider-only syntheses."""
    lwss = lt.pattern("2bo$obo$b2o$7b2o$7bobo$b2o4bo$2b2o$bo!")
    mwss = lt.pattern("o6bo$b2o3bo$2o4b3o3$9b3o$9bo$10bo!")
    hwss = lt.pattern("o$b2o$2o$7b2o$7bobo$2b2o3bo$2bobo$2bo!")
    
    glider_set = gset.extract(comp)
    constell = comp - glider_set.s

    # if the base constellation is already a *WSS salvo, then return it:
    if (constell.centre() == constell[4].centre()):
        return comp

    replaced = lt.pattern()
    matched  = lt.pattern()

    for replacement in [lwss, mwss, hwss, lwss[1], mwss[1], hwss[1]]:
        live = replacement[rewind]
        for o in oriens:
            ro = replacement.transform(o)
            lo = live.transform(o)
            
            m = constell.match(lo, halo="ooo$ooo$ooo!")
            matched  += m.convolve(lo)
            replaced += m.convolve(ro)

    if matched:
        constell -= matched
        return constell[-rewind] + glider_set[-rewind].s + replaced
    else:
        return comp

def encode_comp(comp, remove_spaceships=True, report_cost=False):
    """Encodes the (synthesis) component as a string."""

    if isinstance(comp, str):
        comp = lt.pattern(comp)

    if remove_spaceships:
        comp = remove_standard_spaceships(comp)

    glider_set, constell = comp_canonical_time(comp)

    out_apgcode = comp.oscar(verbose=False, return_apgcode=True, maxexp=100)["apgcode"]

    if out_apgcode == "xs0_0":
        out_apgcode = ""

    if constell:
        period_dict = constell.oscar(return_apgcode=True)
        period, apgcode = period_dict["period"], period_dict["apgcode"]
        cform = lt.pattern(apgcode)
        min_phase, valids = derive_tf(constell, cform, period)
        in_apgcode = f"{apgcode}+{min_phase}" if min_phase > 0 else apgcode
        
        cgs = max([glider_set(*v) for v in valids], key=lambda x: x.pairs())
        res1, res2 = cgs.ct()
        c = f"{in_apgcode}>{res1}{res2}>{out_apgcode}"
        compcost = sum(cgs.ngliders())
    else:
        c = f">{glider_set.ct()[0]}>{out_apgcode}"
        compcost = glider_set.ngliders()
        in_apgcode = "xs0_0"

    if report_cost:
        return c, compcost, in_apgcode, out_apgcode
    else:
        return c

def decode_comp(comp_str):
    """Reconstructs a component from its (canonical) string, without recourse to lifelib.
    Returns (input, number of gliders, output). This is mainly for the search functions."""
    in_data, glider_data, out_data = comp_str.split(">")
    if out_data == "xs0_0": out_data = ""
    apgcode = in_data.partition("+")[0]
    fields = glider_data.partition("@")[0]
    n_gliders = sum(len(part.split()) // 2 for part in fields.split("/"))
    return (apgcode, n_gliders, out_data)

def realise_comp(comp_str, separate=False, advance=-2):
    """Returns a Pattern corresponding to the component string. If separate is True,
    return a Pattern with the input constellation and a glider set; else return a single Pattern,
    ensuring that gliders in the glider set remain disconnected."""
    in_data, glider_data, out_data = comp_str.split(">")
    apgcode, _, phase = in_data.partition("+")
    phase = int(phase) if phase else 0
    base = lt.pattern(apgcode)[phase]
    glider_set = gset.reconstruct(glider_data)
    if separate:
        return (base, glider_set)
    return base[advance] + glider_set[advance].s
