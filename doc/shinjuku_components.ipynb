{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shinjuku import lt\n",
    "from shinjuku.transforms import derive_tf\n",
    "from shinjuku.gliderset import gset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Components and constellations\n",
    "In the context of Shinjuku, a _component_ is a step of a synthesis featuring a glider set hitting a possibly empty constellation. Just as we canonise glider sets, so do we canonise components. Since some interaction _is_ expected in a component, its lifetime is always finite and we can dispense with the techniques needed to handle infinite-lifetime or unidirectional glider sets. (For now, that is.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def comp_canonical_time(comp):\n",
    "    \"\"\"Splits and returns the glider set and the constellation of the component at canonical time.\n",
    "    Assume the gliders are well-spaced and the lifetime is finite.\"\"\"\n",
    "    from itertools import count\n",
    "    glider_set = gset.extract(comp)\n",
    "    constell = comp - glider_set.s\n",
    "    for i in count(1):\n",
    "        advanced = glider_set[i].s\n",
    "        if advanced != glider_set.s[i] or advanced + constell[i] != comp[i]:\n",
    "            return (glider_set[i - 1], constell[i - 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What happens next depends on whether `constell` is empty or not. If the latter, it has a canonical apgcode and the canonical form is such that its bounding box's upper-left corner is at the origin. We now transform the entire component such that the constellation in its present state can be obtained from the canonical form simply by advancing some number of generations, which is minimised; there may be multiple valid transformations.\n",
    "\n",
    "The start of the component string is the canonical apgcode. If it needs to be advanced a non-zero number of generations, `+(min_phase)` follows it.\n",
    "\n",
    "----\n",
    "\n",
    "Each valid transformation sends the glider set to another glider set which produces a `pairs()` output; the transformation chosen as canonical produces the last `pairs()` output in lexicographical order, just like it was for glider sets. The transformed glider set of the canonical transformation is then canonised using `ct()`, and both parts of the result are concatenated to form the middle part of the component string.\n",
    "\n",
    "If `constell` was empty, the glider set is again canonised using `ct()`, but the second item in its output is discarded. This also forms the middle part of the component string.\n",
    "\n",
    "The last part of the component string is the constellation that eventually results from evolving the component, and is represented simply by _its_ canonical apgcode. The three parts – input constellation, glider data and output constellation – are separated by `>` signs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def encode_comp(comp):\n",
    "    \"\"\"Encodes the (synthesis) component as a string.\"\"\"\n",
    "    # For convenience purposes, if comp is a string we convert it into a Pattern\n",
    "    if isinstance(comp, str): comp = lt.pattern(comp)\n",
    "    \n",
    "    glider_set, constell = comp_canonical_time(comp)\n",
    "    out_apgcode = comp.oscar(verbose=False, return_apgcode=True)[\"apgcode\"]\n",
    "    if constell:\n",
    "        period_dict = constell.oscar(return_apgcode=True)\n",
    "        period, apgcode = period_dict[\"period\"], period_dict[\"apgcode\"]\n",
    "        cform = lt.pattern(apgcode)\n",
    "        min_phase, valids = derive_tf(constell, cform, period)\n",
    "        in_apgcode = f\"{apgcode}+{min_phase}\" if min_phase > 0 else apgcode\n",
    "        \n",
    "        cgs = max([glider_set(*v) for v in valids], key=lambda x: x.pairs())\n",
    "        res1, res2 = cgs.ct()\n",
    "        return f\"{in_apgcode}>{res1}{res2}>{out_apgcode}\"\n",
    "    else:\n",
    "        return f\">{glider_set.ct()[0]}>{out_apgcode}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'xs32_y0dbzggcil553xog8oz0343>8 -1/-15 4 14 -2//@0f9 16>xp11_y0dbzggci5ld3xog8oz0343'"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rattlesnake_fs = \"\"\"x = 22, y = 15, rule = B3/S23\n",
    "8b2o$8bo$9bo$8b2o9bo$13bo5bobo$11b2o6b2o$12b2o2b2o$5b2o9bobo$5bobo8bo$\n",
    "2obobobo$ob2obobobo$6bo2b3o$7b2o3bo$9b3o$9bo!\"\"\"\n",
    "fs_str = encode_comp(rattlesnake_fs)\n",
    "fs_str"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Component decoding\n",
    "Since we have already implemented decoding of a glider set, decoding a component is a simple wrapper around that, consisting of peeling off the input constellation and setting it in the Life plane. The output apgcode returned by `decode_comp()` is used when searching for cheapest syntheses, which will be discussed later.\n",
    "\n",
    "`realise_comp()` turns the component into a Pattern that can be displayed. To ensure reversibility of this display, both base and glider set are rewound by two generations before being stuck together."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def decode_comp(comp_str):\n",
    "    \"\"\"Reconstructs a component from its (canonical) string, without recourse to lifelib.\n",
    "    Returns (input, number of gliders, output). This is mainly for the search functions.\"\"\"\n",
    "    in_data, glider_data, out_data = comp_str.split(\">\")\n",
    "    apgcode = in_data.partition(\"+\")[0]\n",
    "    fields = glider_data.partition(\"@\")[0]\n",
    "    n_gliders = sum(len(part.split()) // 2 for part in fields.split(\"/\"))\n",
    "    return (apgcode, n_gliders, out_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def realise_comp(comp_str, separate=False):\n",
    "    \"\"\"Returns a Pattern corresponding to the component string. If separate is True,\n",
    "    return a Pattern with the input constellation and a glider set; else return a single Pattern,\n",
    "    ensuring that gliders in the glider set remain disconnected.\"\"\"\n",
    "    in_data, glider_data, out_data = comp_str.split(\">\")\n",
    "    apgcode, plus, phase = in_data.partition(\"+\")\n",
    "    phase = int(phase) if phase else 0\n",
    "    base = lt.pattern(apgcode)[phase]\n",
    "    glider_set = gset.reconstruct(glider_data)\n",
    "    if separate: return (base, glider_set)\n",
    "    return base[-2] + glider_set[-2].s"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "        <iframe\n",
       "            width=\"512\"\n",
       "            height=\"512\"\n",
       "            src=\"data:text/html;base64,PGh0bWw+PGhlYWQ+PG1ldGEgbmFtZT0iTGlmZVZpZXdlciIgY29udGVudD0icmxlIGNvZGUiPjwvaGVhZD48Ym9keT48ZGl2IGNsYXNzPSJybGUiPjxkaXYgc3R5bGU9ImRpc3BsYXk6bm9uZTsiPjxjb2RlIGlkPSJjb2RlMiI+CiNDTEwgc3RhdGUtbnVtYmVyaW5nIGdvbGx5CnggPSAxNSwgeSA9IDIyLCBydWxlID0gQjMvUzIzCjRiMm8kNWJvJDRibyQ0YjJvMiQ0YjRvJDNibzNibyQyYm9iM28kMmJvOGJvYjJvJDJvYjJvNmIyb2JvJGJvYm8kYgpvYm8kMmJvNmIybyQ4YjJvJDEwYm8yJDZiMm8kNWJvYm8kN2JvJDEwYjJvJDEwYm9ibyQxMGJvIQojQyBbWyBUSEVNRSA2IEdSSUQgR1JJRE1BSk9SIDAgXV0KI0MgW1sgV0lEVEggNDgwIEhFSUdIVCA0ODAgXV0KPC9jb2RlPjwvZGl2Pgo8Y2FudmFzIHdpZHRoPSI0OTYiIGhlaWdodD0iNDk2IiBzdHlsZT0ibWFyZ2luLWxlZnQ6MXB4OyI+PC9jYW52YXM+PC9kaXY+CjxzY3JpcHQgdHlwZT0ndGV4dC9qYXZhc2NyaXB0JyBzcmM9J2h0dHA6Ly93d3cuY29ud2F5bGlmZS5jb20vanMvbHYtcGx1Z2luLmpzJz48L3NjcmlwdD48L2JvZHk+Cgo=\"\n",
       "            frameborder=\"0\"\n",
       "            allowfullscreen\n",
       "        ></iframe>\n",
       "        "
      ],
      "text/plain": [
       "<IPython.lib.display.IFrame at 0x7f994c1bc2e8>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "realise_comp(fs_str).viewer()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "xp30_ya33zyaccz252xeaey8eaex252zg8gxsksy8sksxg8gz01y8ccy81zyaggzya11+21>75 -16 79 -1 83 3 98 -31 110 -15 122 -22 129 1 150 3/9 17 49 12 73 2 103 14 106 -35/75 -17 79 -2 83 2 98 -32 110 -16 122 -23 129 0 150 2/5 18 45 13 69 3 99 15 102 -34@0L15 15>xp47_w32acyeca23zy37y2ccy27z252xeaey8eaex252zg8gxsksy8sksxg8gz01y1oy2ccy2oy11zwggkc01ya10ckggzw1yk1\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "\n",
       "        <iframe\n",
       "            width=\"512\"\n",
       "            height=\"512\"\n",
       "            src=\"data:text/html;base64,PGh0bWw+PGhlYWQ+PG1ldGEgbmFtZT0iTGlmZVZpZXdlciIgY29udGVudD0icmxlIGNvZGUiPjwvaGVhZD48Ym9keT48ZGl2IGNsYXNzPSJybGUiPjxkaXYgc3R5bGU9ImRpc3BsYXk6bm9uZTsiPjxjb2RlIGlkPSJjb2RlMiI+CiNDTEwgc3RhdGUtbnVtYmVyaW5nIGdvbGx5CnggPSA4NCwgeSA9IDgzLCBydWxlID0gQjMvUzIzCjE1Ym8kMTZiMm8kMTViMm82NGJvJDgxYm9ibyQ4MWIybzMkNzVibyQ3M2IybyQ0OWJvMjRiMm8kNDlib2JvJDQ5YgoybyQ1M2JvJDUzYm9ibyQ1M2IybyQzNGJvJDI4Ym9ibzNib2JvJDI5YjJvM2IybyQyOWJvJDY0Ym9ibyQyMmJvYm8KMzRib2JvMmIybyQyM2IybzEzYm9ibzJib2JvMTNiMm80Ym8kMjNibzE1YjJvMmIybzE1Ym8kMzlibzRibzMkNDFiCjJvJDQxYjJvNSQ2N2IzbyQ0MWIybzI0Ym8kNDFiMm8yNWJvJDM3Ym84Ym8kMjhibzdiM282YjNvN2JvJDI3Ym9ibwo1Ym9ib2JvNGJvYm9ibzVib2JvJDI4Ym81YjNvYjNvMmIzb2IzbzVibyQzNWJvYm9ibzRib2JvYm8kMzZiM282YgozbzIkMzZiM282YjNvJDM1Ym9ib2JvNGJvYm9ibyQyOGJvNWIzb2IzbzJiM29iM281Ym8kMjdib2JvNWJvYm9ibwo0Ym9ib2JvNWJvYm8kMjhibzdiM282YjNvN2JvJDM3Ym84Ym8kMTVibzI1YjJvJDE2Ym8yNGIybyQxNGIzbzUkCjQxYjJvJDQxYjJvMyQzOWJvNGJvJDIzYm8xNWIybzJiMm8xNWJvJDE4Ym80YjJvMTNib2JvMmJvYm8xM2IybyQKMThiMm8yYm9ibzM0Ym9ibyQxN2JvYm8kNTRibyQ0OGIybzNiMm8kNDdib2JvM2JvYm8kNDlibyQyOWIybyQyOGJvCmJvJDMwYm8kMzNiMm8kMzJib2JvJDhiMm8yNGJvJDliMm8kOGJvMyRiMm8kb2JvJDJibzY0YjJvJDY2YjJvJDY4YgpvIQojQyBbWyBUSEVNRSA2IEdSSUQgR1JJRE1BSk9SIDAgXV0KI0MgW1sgV0lEVEggNDgwIEhFSUdIVCA0ODAgXV0KPC9jb2RlPjwvZGl2Pgo8Y2FudmFzIHdpZHRoPSI0OTYiIGhlaWdodD0iNDk2IiBzdHlsZT0ibWFyZ2luLWxlZnQ6MXB4OyI+PC9jYW52YXM+PC9kaXY+CjxzY3JpcHQgdHlwZT0ndGV4dC9qYXZhc2NyaXB0JyBzcmM9J2h0dHA6Ly93d3cuY29ud2F5bGlmZS5jb20vanMvbHYtcGx1Z2luLmpzJz48L3NjcmlwdD48L2JvZHk+Cgo=\"\n",
       "            frameborder=\"0\"\n",
       "            allowfullscreen\n",
       "        ></iframe>\n",
       "        "
      ],
      "text/plain": [
       "<IPython.lib.display.IFrame at 0x7f994c40c240>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p30_p47 = lt.pattern(\"\"\"x = 88, y = 87, rule = B3/S23\n",
    "14bobo$15b2o69bo$15bo69bo$85b3o4$78bobo$54bo23b2o$53bo25bo$53b3o$58bo$\n",
    "57bo$57b3o$39bo$28bo9bo$29b2o7b3o$28b2o$70bo$22bo42bo2b2o$23b2o13bo10b\n",
    "o13b2o4b2o$22b2o15b2o6b2o15b2o$38b2o8b2o6$43b2o$43b2o6$43b2o$43b2o27b\n",
    "2o$35b3o12b3o19bobo$30bo26bo14bo$29bobo4bobo10bobo4bobo$30bo8b2o6b2o8b\n",
    "o$38bo10bo4$38bo10bo$30bo8b2o6b2o8bo$29bobo4bobo10bobo4bobo$15bo14bo\n",
    "26bo$13bobo19b3o12b3o$14b2o27b2o$43b2o6$43b2o$43b2o6$38b2o8b2o$22b2o\n",
    "15b2o6b2o15b2o$17b2o4b2o13bo10bo13b2o$18b2o2bo42bo$17bo$58b2o$47b3o7b\n",
    "2o$49bo9bo$48bo$28b3o$30bo$29bo$32b3o$8bo25bo$8b2o23bo$7bobo4$3o$2bo\n",
    "69bo$bo69b2o$71bobo!\"\"\")\n",
    "y = encode_comp(p30_p47)\n",
    "print(y)\n",
    "realise_comp(y).viewer()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Synthesis search utilities\n",
    "The component lines are arranged in files that conventionally have the extension `.sjk`. For longer, manually entered syntheses it is desirable to arrange the steps in chronological order. Anything after # in a line is a comment; empty lines are gracefully ignored.\n",
    "\n",
    "Representing each constellation as a vertex and each component as an edge turns the problem of finding cheapest syntheses into a shortest-path problem which can be solved using Dijkstra's algorithm. `dijkstra()` implements this (or rather a lazy variant which makes the built-in `heapq` sufficient, rather than having to implement a custom min-priority queue) and returns a dictionary of costs and predecessors. This is then passed to `lookup_synth()` to actually obtain the fewest gliders and a Pattern containing all the relevant steps.\n",
    "\n",
    "Where available, common names can be used instead of apgcodes in `lookup_synth()`. Non-alphanumeric characters are ignored, except the full stop disambiguating objects with the same period and population. Orientation locking – transforming synthesis stages to a common coordinate system, then spreading the stages out horizontally – is implemented by default, but can be turned off by passing `orien_lock=False`. The final stage of the synthesis is also oriented to point between east and northeast if it is a spaceship."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "from shinjuku.search import dijkstra, lookup_synth\n",
    "min_paths = dijkstra()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "18\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "\n",
       "        <iframe\n",
       "            width=\"512\"\n",
       "            height=\"512\"\n",
       "            src=\"data:text/html;base64,PGh0bWw+PGhlYWQ+PG1ldGEgbmFtZT0iTGlmZVZpZXdlciIgY29udGVudD0icmxlIGNvZGUiPjwvaGVhZD48Ym9keT48ZGl2IGNsYXNzPSJybGUiPjxkaXYgc3R5bGU9ImRpc3BsYXk6bm9uZTsiPjxjb2RlIGlkPSJjb2RlMiI+CiNDTEwgc3RhdGUtbnVtYmVyaW5nIGdvbGx5CnggPSAxNTgsIHkgPSAyNywgcnVsZSA9IEIzL1MyMwoxNDBibyQxMzhiMm8kMTM5YjJvNCQxMzNibyQ5MWJvYm80MGIybzlibyQ5MmIybzM5YjJvOGIybyQ5MmJvNTFiMm8KNmJvJDk4Ym8zMWIybzIwYm9ibyQ5NmIybzMxYm9ibzIwYjJvJDk3YjJvMzJibyQxNDZibyQ0N2IybzM3YjJvYjJvCjQ1YjJvYjJvM2IybyQ0N2IybzNibzM0Ym9ib2JvNGIybzM4YjJvYm9ibzRiMm84YjJvJDQ1YjJvNGIybzMxYm81YgpvNWJvYm8zNWIybzRibzRibzliMm8kNDViMm80Ym9ibzMwYjJvMTBibzM3YjJvMjFibzMkbzkxYjNvJGIybzNibwo4N2JvNDZiMm8kMm8zYjJvNWIzbzc4Ym80NmJvYm8kNWJvYm80Ym8xMjlibzhiMm8kMTNibzEyMmIybzlibzNib2IKbyQxMzdiMm83YjJvM2JvJDEzNmJvOWJvYm8hCiNDIFtbIFRIRU1FIDYgR1JJRCBHUklETUFKT1IgMCBdXQojQyBbWyBXSURUSCA0ODAgSEVJR0hUIDQ4MCBdXQo8L2NvZGU+PC9kaXY+CjxjYW52YXMgd2lkdGg9IjQ5NiIgaGVpZ2h0PSI0OTYiIHN0eWxlPSJtYXJnaW4tbGVmdDoxcHg7Ij48L2NhbnZhcz48L2Rpdj4KPHNjcmlwdCB0eXBlPSd0ZXh0L2phdmFzY3JpcHQnIHNyYz0naHR0cDovL3d3dy5jb253YXlsaWZlLmNvbS9qcy9sdi1wbHVnaW4uanMnPjwvc2NyaXB0PjwvYm9keT4KCg==\"\n",
       "            frameborder=\"0\"\n",
       "            allowfullscreen\n",
       "        ></iframe>\n",
       "        "
      ],
      "text/plain": [
       "<IPython.lib.display.IFrame at 0x7f9928565eb8>"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "N, pat = lookup_synth(min_paths, \"xq4_go217z1b6koox84szy323\") # crab\n",
    "print(N)\n",
    "pat.viewer()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "19\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "\n",
       "        <iframe\n",
       "            width=\"512\"\n",
       "            height=\"512\"\n",
       "            src=\"data:text/html;base64,PGh0bWw+PGhlYWQ+PG1ldGEgbmFtZT0iTGlmZVZpZXdlciIgY29udGVudD0icmxlIGNvZGUiPjwvaGVhZD48Ym9keT48ZGl2IGNsYXNzPSJybGUiPjxkaXYgc3R5bGU9ImRpc3BsYXk6bm9uZTsiPjxjb2RlIGlkPSJjb2RlMiI+CiNDTEwgc3RhdGUtbnVtYmVyaW5nIGdvbGx5CnggPSAzMjYsIHkgPSAzMywgcnVsZSA9IEIzL1MyMwo4M2JvYm8kODRiMm8kODRibzgkbyRiMm8kMm8xMDBibyQxMWJvOTFiMm8kNmJvYm8yYm9ibzg4YjJvJDJibzNiMm8KM2IybzEzMGIybzdibzQxYjJvNjFiMm81NGIybyRvYm80Ym8xMzVibzhib2JvMzlibzYyYm81NWJvJGIybzQxYm9iCjJvMzdib2Iyb2IybzliMm8zMmJvYjJvYjJvYm84YjJvMzJib2Iyb2Iyb2JvNTRib2Iyb2Iyb2JvNDdib2Iyb2IybwpibyQ0NGIyb2JvM2JvMzNiMm9ib2JvYm85YjJvMzFiMm9ib2JvYm80M2Iyb2JvYm9ibzU1YjJvYm9ib2JvNDhiMm8KYm9ib2JvJDUwYjJvMzlibzlibzM5Ym81MGJvMTNibzQ4Ym8xM2JvNDFibzEzYm8kNTBib2JvMTAxYjJvNDhiM28KNjBiM281M2IzbyQxNTNiMm80OGJvNjJibzU1Ym8kMTU1Ym80N2IybzYxYjJvNTRiMm8kMTQ4YjJvMTYxYjJvJAoxNDliMm8xNDlib2IybzZiMm8kMTQ4Ym8xMDBiMm8xNGIybzMzYjJvYm84Ym84YjJvJDIwMWIybzQxYm80Ym9ibwoxM2IybzU0YjJvJDIwMGJvYm8zNmIybzNiMm8zYm81N2IzbyQyMDJibzM1Ym9ibzJib2JvNjNibyQyMDViMm8zM2IKbzY3Ym8kMjA1Ym9ibzQyYjJvJDIwNWJvNDNiMm8kMjUxYm8hCiNDIFtbIFRIRU1FIDYgR1JJRCBHUklETUFKT1IgMCBdXQojQyBbWyBXSURUSCA0ODAgSEVJR0hUIDQ4MCBdXQo8L2NvZGU+PC9kaXY+CjxjYW52YXMgd2lkdGg9IjQ5NiIgaGVpZ2h0PSI0OTYiIHN0eWxlPSJtYXJnaW4tbGVmdDoxcHg7Ij48L2NhbnZhcz48L2Rpdj4KPHNjcmlwdCB0eXBlPSd0ZXh0L2phdmFzY3JpcHQnIHNyYz0naHR0cDovL3d3dy5jb253YXlsaWZlLmNvbS9qcy9sdi1wbHVnaW4uanMnPjwvc2NyaXB0PjwvYm9keT4KCg==\"\n",
       "            frameborder=\"0\"\n",
       "            allowfullscreen\n",
       "        ></iframe>\n",
       "        "
      ],
      "text/plain": [
       "<IPython.lib.display.IFrame at 0x7f994c1bc3c8>"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "N, pat = lookup_synth(min_paths, \"Tanner's p46\")\n",
    "print(N)\n",
    "pat.viewer()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`consistency_check()` is a utility to check whether all components\n",
    "\n",
    "* do as they say, i.e. they produce the expected output constellation\n",
    "* represent valid synthesis steps, i.e. are infinitely rewindable\n",
    "\n",
    "The GitLab CI calls this function upon every commit, which is also handy for catching typos, missed characters and general sloppiness in the component files."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
