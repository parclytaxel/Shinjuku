# Installing Shinjuku on Windows 10

Here are some notes written up by Dave Greene about getting Shinjuku working on a Windows 10 machine. Things are a bit different now that Python 3.x is Golly-standard, Python 2.x is obsolete, and lifelib has been upgraded to v2.4.4, removing the Python2 dependency.

## Background and Thank You
{Dave says:}
For most of a year I had been unable to get a copy of the Shinjuku project into a state on my computer where I could even read the documentation, let alone understand it or use Shinjuku. This was in large part because the only docs were in the form of Jupyter notebooks, which are quite magical and wonderful if they work, but very mysterious and error-filled if they don't. With a lot of help from Adam P. Goucher, I was able to get everything in working order -- thanks, Adam! 

## Getting started

Type `cmd` [Enter] in the Start Bar search field to get a Windows command prompt.

You can do this optional update, just for the record:
```cd c:\python39\scripts
c:\python39\python.exe -m pip install --upgrade pip
```
Then try

`pip install notebook`

followed by

`pip install --user --upgrade python-lifelib`
  
Some warnings may come up that look like this:
```
WARNING: The scripts jupyter-migrate.exe, jupyter-troubleshoot.exe and jupyter.exe are installed in 'c:\python39\Scripts' which is not on PATH.
Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  
WARNING: The script tqdm.exe is installed in 'C:\Users\{username}\AppData\Roaming\Python\Python39\Scripts' which is not on PATH.
Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
```

### Adding folders to PATH environment variable
If you want to be able to just type `pip install` or `jupyter notebook` from any folder you happen to be looking at in the command window, it might be a good idea to follow that advice (if it comes up for you).  Walk through the instructions at
    https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/
  to add `C:\python39\Scripts`, or whatever path is reported to you, to the PATH environment variable -- the system variable, not the user variable.

If this seems to end up causing any trouble on your system (which it shouldn't) you can follow the same steps to remove any additional paths you've added to PATH.
 
## Install lifelib
Okay, now to install lifelib!  It looks like it would probably work to follow the lifelib readme instructions and clone lifelib as a folder inside the shinjuku folder.
  
It seems safer to follow the "install system-wide" instructions instead, because it mentions Jupyter notebooks.  

https://gitlab.com/apgoucher/lifelib/blob/master/doc/install.md

`pip install --user --upgrade python-lifelib` will install in an ugly subsubsubfolder belonging to your specific Windows user account, like the tqdm WARNING: shown above.

Conversely,

`pip install --upgrade python-lifelib` will install lifelib system-wide, available to any user.

I chose the second option because **C:\Users\{username}\AppData\Roaming\Python\Python39\Scripts** just seems like a horrible place to put anything if you ever want to find it again.  Side note, the "AppData" folder is a hidden folder, so you won't see it by default if you use File Explorer to go to `C:\Users\{username}`.  The folder is still there, though, so if you type in the path to the File Explorer path bar, it will work perfectly fine.

## Install numpy
Similarly, `numpy` can be installed system-wide, or under your particular Windows user account:

`pip install numpy`
or
`pip install --user numpy`

Note that if you're not sure if something has been installed, it's safe to run the same command again:
```pip install numpy
Requirement already satisfied: numpy in c:\python39\lib\site-packages (1.21.2)
```
I installed `numpy` system-wide, as before.  I'm actually not sure that this is necessary, because numpy will also end up getting installed inside the Cygwin installation of Python 3.8 -- and I think that's where numpy actually gets used.  Just including the above in case it turns out to be needed.

## Install numpy and other dependencies in Cygwin
The simplest way to make sure that numpy is installed inside Cygwin is by typing the following in your command-line window (Windows install, not Cygwin) and typing

`C:\Python39\python`

(and hitting [Enter].)  This will get you into Python interactive mode, which is marked by triple >s instead of a path and a single > -- like `>>>` instead of, e.g., `C:\Users\{username}>`.

In Python interactive mode, type the following lines:
```
import lifelib
lifelib.install_cygwin('C:/path/to/your/cygwin64/')
```

(whatever that is on your system)

After a certain amount of mysterious delay while `install_cygwin()` downloads things, this will pop up a new non-interactive window that will automatically install the necessary dependencies into the chosen folder.  On my particular system, that folder is `C:\cygwin64`, but you may have chosen something different, so adjust accordingly!

This is all if you already have Cygwin installed and you want to update it.  Again, the idea is to pass in the path to that installation, whatever it is on your system.  Otherwise, use just plain 

`lifelib.install_cygwin()`

which will install a whole new copy of Cygwin inside lifelib's installation directory.  (This seems fine if you aren't using Cygwin for anything else, but otherwise it seems rather messy.)

In either case, when that Cygwin installer pop-up window disappears, you can type

`exit()`

to get back out of Python interactive mode.

### Pay Attention Here Especially If You're Having Problems
This point about a secondary Cygwin-based install of Python and `numpy` is worth emphasizing.  When I ran the python-lifelib install above, it definitely said it was installing `numpy`.  However, when I tried to run `shinjuku_glidersets.ipynb`, I got an error:
  
```
Exception ignored in: <function Pattern.__del__ at 0x000001EA8C04FA60>
  Traceback (most recent call last):
    File "C:\Users\greedd\AppData\Roaming\Python\Python39\site-packages\lifelib\pythlib\pattern.py", line 40, in __del__
      self.lifelib('DeletePattern', self.ptr)
    File "C:\Users\greedd\AppData\Roaming\Python\Python39\site-packages\lifelib\pythlib\lowlevel.py", line 205, in __call__
      self.the_library.stdin.flush()
OSError: [Errno 22] Invalid argument
```
and the command window that's running Jupyter Notebook shows an apparent problem:

```
Traceback (most recent call last):
  File "pythlib/lowlevel.py", line 247, in <module>
    remote_load()
  File "pythlib/lowlevel.py", line 160, in remote_load
    obj = pickle.load(stdin)
ModuleNotFoundError: No module named 'numpy'
```
I had to follow the instructions above to install another copy of numpy *inside Cygwin's Python38 installation.*

## Time to Open the Jupyter Notebooks (Finally)!
Go to your repository folder in Windows File Explorer.  Press Alt+D, or just click in the File Explorer path bar, to select the current path.  Simply type `cmd` right over the top of the highlighted path and hit **[Enter]**, and you'll get a command window with the directory already changed to that particular folder.

Alternatively, of course, if you already have a command window open, you can type:

`cd C:\path\to\your\shinjuku\repo\doc`

(i.e., replace this with wherever you have cloned the shinjuku repo, and end with `\doc`)

Type and hit **[Enter]**:

`jupyter notebook`

A browser window should pop up, containing a list of the three documentation files

```
shinjuku_components.ipynb
shinjuku_glidersets.ipynb
shinjuku_templates.ipynb
```
You're supposed to look at the `shinjuku_glidersets` one first, so double-click to open that one.

Add the line

`lifelib.add_cygdir('C:/cygwin64/')`

just below the `import lifelib` line at the beginning of this Jupyter notebook in Shinjuku's doc folder -- and do the same for each of these notebooks when you open them.

## Now, Run the Jupyter Notebooks!
Jupyter notebooks can be run by selecting particular code blocks and hitting the Run button at the top -- looks like a standard right-pointing triangle Play button.  You don't have to do any selecting, though -- just start clicking the Run button repeatedly, and Jupyter will run the next executable block, then the next, etc., all the way to the end, scrolling down automatically as you go.

The first time you hit the Run button at the top of a notebook, you'll probably see an ugly error:

```
Exception ignored in: <function tqdm.__del__ at 0x00000197E9D94940>
Traceback (most recent call last):
  File "c:\python39\lib\site-packages\tqdm\std.py", line 1152, in __del__
    self.close()
  File "c:\python39\lib\site-packages\tqdm\notebook.py", line 286, in close
    self.disp(bar_style='danger', check_delay=False)
AttributeError: 'tqdm' object has no attribute 'disp'
```
This is okay, as long as it's followed by some evidence that lifelib is getting to work:

```
Generating code for rules ['b3s23']...
Compiling lifelib shared object (estimated time: X seconds)...
[=     ]===============================================================
```
Once the equal signs stop appearing, re-click on the In [1] code block at the top, and click Run again.  All these ugly messages should go away.

Keep clicking on the **Run** button (again, no need to select anything in the notebook, it will just go on to the next).  If all goes well, LifeViewer canvases will pop up and get populated for each s.viewer() call in the Python code.

## This Is Only The Beginning...
Jupyter notebooks, along with lifelib's clever integration with LifeViewer, make a very good tool for doing exploration and documentation in all sorts of different areas of CA research, from writing experimental search programs to assembling large self-constructing or calculation circuitry in various rules.  These Shinjuku documents, once they're in working order, give a very nice example of how Python lifelib calls can be integrated into interactive LifeViewer canvases.
